import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'splashscreen',
    loadChildren: () => import('./pages/splashscreen/splashscreen.module').then( m => m.SplashscreenPageModule)
  },
  {
    path: '',
    redirectTo: 'splashscreen',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'button1',
    loadChildren: () => import('./button1/button1.module').then( m => m.Button1PageModule)
  },
  {
    path: 'button2',
    loadChildren: () => import('./button2/button2.module').then( m => m.Button2PageModule)
  },
  {
    path: 'button3',
    loadChildren: () => import('./button3/button3.module').then( m => m.Button3PageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
