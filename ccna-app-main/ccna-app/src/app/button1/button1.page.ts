import { Component } from '@angular/core';

@Component({
  selector: 'app-button1',
  templateUrl: './button1.page.html',
  styleUrls: ['./button1.page.scss'],
})
export class Button1Page {
  tutorialList: Tutorial[] = [
    { title: 'Tutorial 1', description: 'This is tutorial 1' },
    { title: 'Tutorial 2', description: 'This is tutorial 2' },
    { title: 'Tutorial 3', description: 'This is tutorial 3' },
  ];

  onTutorialClick(tutorial: Tutorial) {
    console.log(`Clicked tutorial: ${tutorial.title}`);
    // Perform any desired action when a tutorial is clicked
  }
}

interface Tutorial {
  title: string;
  description: string;
}