import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { parseString } from 'xml2js';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class XmlDataService {
  constructor(private http: HttpClient) {}

  fetchXmlData(xmlUrl: string): Observable<any> {
    return this.http.get(xmlUrl, { responseType: 'text' });
  }

  parseXmlData(xmlString: string): Promise<any> {
    return new Promise((resolve, reject) => {
      parseString(xmlString, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  retrieveItemDetails(xmlData: any, itemIds: string[]): any[] {
    const items = xmlData.data.item;
    const itemDetails: any[] = [];

    itemIds.forEach((itemId) => {
      const item = items.find((item: any) => item.$.id === itemId);
      if (item) {
        itemDetails.push(item);
      } else {
        console.error(`Item with ID ${itemId} not found`);
      }
    });

    return itemDetails;
  }
}
