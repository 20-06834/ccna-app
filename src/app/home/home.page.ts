import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { App } from '@capacitor/app';
import { HttpClient } from '@angular/common/http';
import { parseString } from 'xml2js';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  xmlData: any;
  itemDetails: any[] = [];

  constructor(private navCtrl: NavController, private http: HttpClient) {}

  onButton1Click() {
    // Logic for button 1 click
    this.navCtrl.navigateForward('button1');
  }

  onButton2Click() {
    // Logic for button 2 click
    this.navCtrl.navigateForward('button2');
  }

  onButton3Click() {
    // Logic for button 3 click
    this.navCtrl.navigateForward('button3');
  }

  onExitClick() {
    // Exit the application
    App.exitApp();
  }

  fetchXmlData() {
    this.http.get('path/to/data.xml', { responseType: 'text' }).subscribe((xmlString) => {
      parseString(xmlString, (err, result) => {
        if (err) {
          console.error('Error parsing XML:', err);
        } else {
          this.xmlData = result;
          this.retrieveItemDetails();
        }
      });
    });
  }

  retrieveItemDetails() {
    const items = this.xmlData.data.item;
    const itemIds = ['1', '2']; // Set the array of IDs you want to retrieve
    itemIds.forEach((itemId) => {
      const item = items.find((item: any) => item.$.id === itemId);
      if (item) {
        this.itemDetails.push(item);
      } else {
        console.error(`Item with ID ${itemId} not found`);
      }
    });
  }

  ngOnInit() {
    this.fetchXmlData();
  }
}
